from rest_framework import permissions


class IsObjectOwnerCurrentUser(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if hasattr(obj, 'owner'):
            return obj.owner == request.user
        if hasattr(obj, 'user'):
            return obj.user == request.user


class CreateUpdateDeleteIsAuthenticated(permissions.IsAuthenticated):
    def has_permission(self, request, view):
        return (view.action == 'list'
                or super(CreateUpdateDeleteIsAuthenticated, self).has_permission(request, view))
