from django.contrib.auth.models import AbstractUser
from django.db import models


class CustomUser(AbstractUser):
    lang_key = models.CharField(max_length=200, default='en')
    image_url = models.CharField(max_length=200, default='')

    def __str__(self):
        return self.email

# class UserBooking(models.Model):
#     user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
#     listing = models.ForeignKey(Listing, on_delete=models.CASCADE)
#     date_arrive = models.DateField()
#     date_arrive = models.DateField()
#
#     path = models.CharField(max_length=200)
