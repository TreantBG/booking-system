from rest_framework import serializers

from users.models import CustomUser


class UserSerializer(serializers.ModelSerializer):
    langKey = serializers.CharField(source='lang_key')
    activated = serializers.CharField(source='is_active')
    firstName = serializers.CharField(source='first_name')
    lastName = serializers.CharField(source='last_name')
    login = serializers.CharField(source='username')
    imageUrl = serializers.CharField(source='image_url')

    class Meta:
        model = CustomUser
        fields = (
            'id',
            'activated',
            'firstName',
            'lastName',
            'email',
            'langKey',
            'login',
            'imageUrl',
        )
