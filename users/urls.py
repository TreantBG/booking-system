from django.urls import path

from users import views

urlpatterns = [
    path('', views.CurrentUserView.as_view()),
    path('list/', views.UserList.as_view()),
    path('<int:pk>/', views.UserDetail.as_view()),
]
