from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models import CustomUser


class CustomUserAdmin(UserAdmin):
    model = CustomUser
    list_display = ['email', 'username']
    fieldsets = UserAdmin.fieldsets + (
        (None, {'fields': ('lang_key', 'image_url')}),
    )


admin.site.register(CustomUser, CustomUserAdmin)
