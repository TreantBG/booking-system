from rest_framework import permissions
from rest_framework import viewsets, generics

from django_demo.permissions import IsObjectOwnerCurrentUser, CreateUpdateDeleteIsAuthenticated
from listings.mixins import ImageListMixin
from .models import Listing, Room, Payment, Bed, Amenity, Image
from .serializers import ListingSerializer, RoomSerializer, PaymentSerializer, BedSerializer, AmenitySerializer, \
    ImageSerializer


class ListingsViewSet(viewsets.ModelViewSet):
    queryset = Listing.objects.all()
    serializer_class = ListingSerializer
    permission_classes = (IsObjectOwnerCurrentUser, CreateUpdateDeleteIsAuthenticated,)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class RoomViewSet(viewsets.ModelViewSet):
    queryset = Room.objects.all()
    serializer_class = RoomSerializer
    permission_classes = (IsObjectOwnerCurrentUser, CreateUpdateDeleteIsAuthenticated,)


class PaymentViewSet(viewsets.ModelViewSet):
    queryset = Payment.objects.all()
    serializer_class = PaymentSerializer
    http_method_names = ['get', ]


class BedViewSet(viewsets.ModelViewSet):
    queryset = Bed.objects.all()
    serializer_class = BedSerializer
    http_method_names = ['get', ]


class AmenityViewSet(viewsets.ModelViewSet):
    queryset = Amenity.objects.all()
    serializer_class = AmenitySerializer
    http_method_names = ['get', ]


class ImageViewSet(ImageListMixin, viewsets.ModelViewSet):
    queryset = Image.objects.all()
    serializer_class = ImageSerializer
    permission_classes = (CreateUpdateDeleteIsAuthenticated, IsObjectOwnerCurrentUser,)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class ImageFilterByUserList(generics.ListAPIView):
    serializer_class = ImageSerializer

    def get_queryset(self):
        user_id = int(self.kwargs['user_id'])
        return Image.objects.filter(user=user_id)
