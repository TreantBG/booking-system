from django.contrib import admin

from .models import Amenity, Bed, Payment, Room, Listing, Image


class AmenityAdmin(admin.ModelAdmin):
    search_fields = ['type']


class BedAdmin(admin.ModelAdmin):
    search_fields = ['type']


class PaymentAdmin(admin.ModelAdmin):
    search_fields = ['type']


class RoomAdmin(admin.ModelAdmin):
    search_fields = ['description']


class ListingAdmin(admin.ModelAdmin):
    search_fields = ['description']


class ImagesAdmin(admin.ModelAdmin):
    search_fields = ['user']


admin.site.register(Amenity, AmenityAdmin)
admin.site.register(Bed, BedAdmin)
admin.site.register(Payment, PaymentAdmin)
admin.site.register(Room, RoomAdmin)
admin.site.register(Listing, ListingAdmin)
admin.site.register(Image, ImagesAdmin)
