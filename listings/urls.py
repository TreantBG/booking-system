from django.conf.urls import url
from rest_framework import routers

from listings import views

router = routers.DefaultRouter()
router.register(r'listings', views.ListingsViewSet)
router.register(r'rooms', views.RoomViewSet)
router.register(r'payments', views.PaymentViewSet)
router.register(r'beds', views.BedViewSet)
router.register(r'amenities', views.AmenityViewSet)
router.register(r'images', views.ImageViewSet)

urlpatterns = [
                  url('^images/user/(?P<user_id>.+)/$', views.ImageFilterByUserList.as_view()),
              ] + router.urls
