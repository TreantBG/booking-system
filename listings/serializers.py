from rest_framework import serializers

from .models import Listing, Room, Payment, Bed, Amenity, Image


class RoomSerializer(serializers.ModelSerializer):
    class Meta:
        model = Room
        fields = '__all__'


class ListingSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.id')

    rooms = serializers.SerializerMethodField()

    class Meta:
        model = Listing
        fields = '__all__'

    def get_rooms(self, obj):
        queryset = Room.objects.filter(listing=obj.id)
        serializer = RoomSerializer(queryset, many=True)
        return serializer.data


class PaymentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Payment
        fields = '__all__'


class BedSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bed
        fields = '__all__'


class AmenitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Amenity
        fields = '__all__'


class ImageSerializer(serializers.ModelSerializer):
    user = serializers.ReadOnlyField(source='user.id')

    class Meta:
        model = Image
        fields = '__all__'
