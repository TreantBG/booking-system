from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

from django_demo import settings


class Image(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    type = models.CharField(max_length=200)
    path = models.CharField(max_length=200)

    def __str__(self):
        return self.name + " " + self.type + " " + self.path


class Amenity(models.Model):
    type = models.CharField(max_length=200, blank=True)
    text = models.CharField(max_length=200)
    icon = models.CharField(max_length=200)

    def __str__(self):
        return self.text + "  " + self.icon


class Bed(models.Model):
    type = models.CharField(max_length=200, blank=True)
    text = models.CharField(max_length=200)
    icon = models.CharField(max_length=200)

    def __str__(self):
        return self.text + "  " + self.icon


class Payment(models.Model):
    type = models.CharField(max_length=200, blank=True)
    text = models.CharField(max_length=200)
    icon = models.CharField(max_length=200)

    def __str__(self):
        return self.text + "  " + self.icon


class Listing(models.Model):
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    description = models.CharField(max_length=400, blank=True)
    amenities = models.ManyToManyField(Amenity, blank=True)
    payments = models.ManyToManyField(Payment, blank=True)
    images = models.ManyToManyField(Image, blank=True)
    createdDate = models.DateTimeField(auto_now_add=True)
    lastModifiedDate = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name + " " + str(self.id)


class Room(models.Model):
    listing = models.ForeignKey(Listing, on_delete=models.CASCADE)
    floor = models.IntegerField()
    description = models.CharField(max_length=200)
    max_people = models.IntegerField()
    price = models.FloatField()
    available = models.BooleanField(default=True)
    beds = models.ManyToManyField(Bed, blank=True)
    createdDate = models.DateTimeField(auto_now_add=True)
    lastModifiedDate = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.description


class Feedback(models.Model):
    listing = models.ForeignKey(Listing, on_delete=models.CASCADE)
    rating_overall = models.IntegerField(validators=[MaxValueValidator(10), MinValueValidator(1)])
    rating_cleanness = models.IntegerField(validators=[MaxValueValidator(10), MinValueValidator(1)])
    rating_comfort = models.IntegerField(validators=[MaxValueValidator(10), MinValueValidator(1)])
    rating_location = models.IntegerField(validators=[MaxValueValidator(10), MinValueValidator(1)])
    rating_staff = models.IntegerField(validators=[MaxValueValidator(10), MinValueValidator(1)])
    rating_price_quality = models.IntegerField(validators=[MaxValueValidator(10), MinValueValidator(1)])
    rating_wifi = models.IntegerField(validators=[MaxValueValidator(10), MinValueValidator(1)])

    comment = models.CharField(max_length=400, blank=True)
