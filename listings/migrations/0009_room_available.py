# Generated by Django 2.1.2 on 2018-11-04 09:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('listings', '0008_auto_20181104_1120'),
    ]

    operations = [
        migrations.AddField(
            model_name='room',
            name='available',
            field=models.BooleanField(default=True),
        ),
    ]
