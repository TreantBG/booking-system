from django.db import models

from django_demo import settings


class Booking(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    listing = models.ForeignKey('listings.Listing', on_delete=models.CASCADE)
    date_arrive = models.DateTimeField()
    date_leave = models.DateTimeField()
    guests = models.IntegerField()
    smokers = models.BooleanField(default=False, blank=True)
    special_requirements = models.CharField(max_length=400, blank=True)
    createdDate = models.DateTimeField(auto_now_add=True)
    lastModifiedDate = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.listing.name + " " + str(self.date_arrive)
