from django.conf.urls import url
from rest_framework import routers

from booking import views

router = routers.DefaultRouter()
router.register(r'booking', views.BookingViewSet)

urlpatterns = [
                  url('^booking/listing/(?P<listing_id>.+)/$', views.BookingFilterByListingList.as_view()),
              ] + router.urls
