from django.shortcuts import render
from rest_framework import viewsets, permissions, generics

from booking.serializers import BookingSerializer
from django_demo.permissions import IsObjectOwnerCurrentUser
from .models import Booking


class BookingViewSet(viewsets.ModelViewSet):
    queryset = Booking.objects.all()
    serializer_class = BookingSerializer
    permission_classes = (permissions.IsAuthenticated, IsObjectOwnerCurrentUser,)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

class BookingFilterByListingList(generics.ListAPIView):
    serializer_class = BookingSerializer

    def get_queryset(self):
        listing_id = int(self.kwargs['listing_id'])
        return Booking.objects.filter(listing=listing_id)
