from django.contrib import admin

from .models import Booking


class BookingyAdmin(admin.ModelAdmin):
    search_fields = ['user']


admin.site.register(Booking, BookingyAdmin)
